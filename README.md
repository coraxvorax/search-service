# Searcher service #

### Install ###

* using java 8

To start service you may

* execute command:

~~~
java -server -classpath $SERVICE_HOME/lib/*:$SERVICE_HOME/conf:$SERVICE_HOME/data ru.searcher.SearcherService
~~~

* or using wrapper:

~~~
$SERVICE_HOME/asService/searcher.sh start
~~~

or:
~~~
$SERVICE_HOME/asService/wrapper -c $SERVICE_HOME/asService/service.properties
~~~

* using docker


You should write to file /$SEARCH_SERVICE_DIST/conf/config.main.yml parameter:

~~~
jsonFilePath: /app/data/data.json
~~~

You should execute command:

~~~
docker build -t search_service .
~~~

and

~~~
docker run --name search_service_1 -v /$SEARCH_SERVICE_DIST/conf/:/app/conf \
                                   -v /path/to/log/dir/:/LOGS  \
                                   -p 7883:7883 -p 10005:10005 \
                                   -d search_service
~~~

### Testing ###

For testing you should send POST request to http://SERVICE_HOST:7883/applicants/search/document

~~~
{
    "text":"search text"
}
~~~



### Not yet done ###

* frontend
* maybe some tests
* correct search by lucene searcher