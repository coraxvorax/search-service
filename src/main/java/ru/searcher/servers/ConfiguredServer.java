package ru.searcher.servers;

import com.google.inject.servlet.CustomGuiceFilter;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.DefaultServlet;
import ru.searcher.servers.jetty.JettyWebServer;
import ru.searcher.servers.jetty.WebServerException;

import java.util.EventListener;


public class ConfiguredServer extends JettyWebServer {

    public ConfiguredServer(String config,
                            EventListener eventListener,
                            String logConfig) throws WebServerException {
        super(config, logConfig);
        getServletContext().addEventListener(eventListener);
    }

    public void configure() throws WebServerException {
        getServletContext().addFilter(CustomGuiceFilter.class, "/*", null);
        getServletContext().addServlet(DefaultServlet.class, "/");
    }

    @Override
    protected void initSessionIdManager(Server server) {
        //NOP
    }
}
