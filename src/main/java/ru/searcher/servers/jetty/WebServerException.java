package ru.searcher.servers.jetty;


public class WebServerException extends Exception {
    private static final long serialVersionUID = 2388326449787550521L;

    public WebServerException(String message) {
        super(message);
    }

    public WebServerException(Throwable cause) {
        super(cause);
    }
}

