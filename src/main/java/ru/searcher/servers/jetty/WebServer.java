package ru.searcher.servers.jetty;

import java.util.EventListener;

public interface WebServer {

    void start() throws WebServerException;

    void stop() throws WebServerException;

    void configure() throws WebServerException;

    void addEventListener(EventListener listener);
}
