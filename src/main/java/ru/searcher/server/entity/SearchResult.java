package ru.searcher.server.entity;

import java.util.List;


public class SearchResult {

    private List<DetectedDoc> detectedDocs;

    public SearchResult() {
    }

    public SearchResult(List<DetectedDoc> detectedDocs) {
        this.detectedDocs = detectedDocs;
    }

    public List<DetectedDoc> getDetectedDocs() {
        return detectedDocs;
    }

    @Override
    public String toString() {
        return "SearchResult{" +
                "detectedDocs=" + detectedDocs +
                '}';
    }
}
