package ru.searcher.server.entity;

import com.fasterxml.jackson.annotation.JsonProperty;


public class SearchRequest {
    @JsonProperty("text")
    private String text;

    public String getText() {
        return text;
    }

    public SearchRequest() {
    }

    public SearchRequest(String text) {
        this.text = text;
    }
}
