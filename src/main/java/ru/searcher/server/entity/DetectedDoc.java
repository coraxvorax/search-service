package ru.searcher.server.entity;

import com.fasterxml.jackson.annotation.JsonProperty;


public class DetectedDoc {
    @JsonProperty("Name")
    private String name;

    @JsonProperty("Type")
    private String type;

    @JsonProperty("Designed By")
    private String designedBy;

    public DetectedDoc() {
    }

    public DetectedDoc(String name, String type, String designedBy) {
        this.name = name;
        this.type = type;
        this.designedBy = designedBy;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getDesignedBy() {
        return designedBy;
    }

    @Override
    public String toString() {
        return "DataDoc{" +
                "name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", designedBy='" + designedBy + '\'' +
                '}';
    }
}
