package ru.searcher.server.api;

import ru.searcher.search.SearchDoc;
import ru.searcher.search.Searcher;
import ru.searcher.server.APIException;
import ru.searcher.server.entity.DetectedDoc;
import ru.searcher.server.entity.SearchRequest;
import ru.searcher.server.entity.SearchResult;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;


@Path("/search/")
public class SearchAPI {
    @Inject
    private Searcher searcher;

    @POST
    @Path("/document/")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public SearchResult statusDetail(SearchRequest searchRequest) {
        try {
            final List<DetectedDoc> resultList = new ArrayList<>();
            for (SearchDoc doc : searcher.findDocs(searchRequest.getText())) {
                resultList.add(new DetectedDoc(doc.getName(), doc.getType(), doc.getDesignedBy()));
            }
            return new SearchResult(resultList);
        } catch (Exception e) {
            throw new APIException("Internal error");
        }
    }
}
