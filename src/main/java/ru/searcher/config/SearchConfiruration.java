package ru.searcher.config;

import org.yaml.snakeyaml.Yaml;
import ru.searcher.SearcherService;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;

public class SearchConfiruration implements Configuration {

    private final MappedConfig config;

    public SearchConfiruration(Path configPath) throws IOException, URISyntaxException {
        try (InputStream in = Files.newInputStream(configPath)) {
            this.config = new Yaml().loadAs(in, MappedConfig.class);
        }
    }

    @Override
    public String getIndexPath() {
        return config.indexPath;
    }

    @Override
    public String getJSONFilePath() {
        return config.jsonFilePath;
    }

    @Override
    public SearcherService.SearchStrategy getSearchStrategy() {
        return SearcherService.SearchStrategy.valueOf(config.searchStrategy);
    }

    public static class MappedConfig {
        private String prefixServletName;
        private String indexPath;
        private String jsonFilePath;
        private String searchStrategy;

        public String getPrefixServletName() {
            return prefixServletName;
        }

        public void setPrefixServletName(String prefixServletName) {
            this.prefixServletName = prefixServletName;
        }

        public String getIndexPath() {
            return indexPath;
        }

        public void setIndexPath(String indexPath) {
            this.indexPath = indexPath;
        }

        public String getJsonFilePath() {
            return jsonFilePath;
        }

        public void setJsonFilePath(String jsonFilePath) {
            this.jsonFilePath = jsonFilePath;
        }

        public String getSearchStrategy() {
            return searchStrategy;
        }

        public void setSearchStrategy(String searchStrategy) {
            this.searchStrategy = searchStrategy;
        }
    }
}
