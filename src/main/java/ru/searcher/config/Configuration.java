package ru.searcher.config;


import ru.searcher.SearcherService;

public interface Configuration {

    String getIndexPath();

    String getJSONFilePath();

    SearcherService.SearchStrategy getSearchStrategy();
}
