package ru.searcher;

import org.tanukisoftware.wrapper.WrapperListener;
import org.tanukisoftware.wrapper.WrapperManager;


public class SearcherServiceWrapper implements WrapperListener {


    private SearcherService searcherService;

    @Override
    public Integer start(String[] params) {
        searcherService = new SearcherService();
        searcherService.init(params);
        return null;
    }

    @Override
    public int stop(int i) {
        searcherService.stop();
        return 0;
    }

    @Override
    public void controlEvent(int event) {
        if ((event == WrapperManager.WRAPPER_CTRL_LOGOFF_EVENT)
                && (WrapperManager.isLaunchedAsService())) {
        } else {
            WrapperManager.stop(0);
        }
    }

    public static void main(String[] args) {
        WrapperManager.start(new SearcherServiceWrapper(), args);
    }
}
