package ru.searcher.guice;

import com.google.inject.Injector;
import com.google.inject.Provider;
import ru.searcher.config.Configuration;
import ru.searcher.search.Indexer;
import ru.searcher.search.lucene.LuceneIndexer;
import ru.searcher.search.simple.SimpleIndexer;
import ru.searcher.search.simple.SimpleStore;

import javax.inject.Inject;


public class IndexerProvider implements Provider<Indexer> {

    @Inject
    private Configuration configuration;

    @Inject
    private Injector injector;

    @Override
    public Indexer get() {
        switch (configuration.getSearchStrategy()) {
            case LUCENE:
                return new LuceneIndexer(configuration);
            case SIMPLE:
                return new SimpleIndexer(
                        injector.getInstance(SimpleStore.class),
                        configuration
                );
            default:
                return new SimpleIndexer(
                        injector.getInstance(SimpleStore.class),
                        configuration
                );

        }
    }
}
