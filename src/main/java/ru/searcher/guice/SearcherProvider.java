package ru.searcher.guice;

import com.google.inject.Injector;
import com.google.inject.Provider;
import ru.searcher.config.Configuration;
import ru.searcher.search.Searcher;
import ru.searcher.search.lucene.LuceneSearcher;
import ru.searcher.search.simple.SimpleSearcher;
import ru.searcher.search.simple.SimpleStore;

import javax.inject.Inject;

/**
 * Created by user on 08.06.2017.
 */
public class SearcherProvider implements Provider<Searcher> {

    @Inject
    private Configuration configuration;

    @Inject
    private Injector injector;

    @Override
    public Searcher get() {
        switch (configuration.getSearchStrategy()) {
            case LUCENE:
                return new LuceneSearcher(configuration);
            case SIMPLE:
                return new SimpleSearcher(injector.getInstance(SimpleStore.class));
            default:
                return new SimpleSearcher(injector.getInstance(SimpleStore.class));

        }
    }
}
