package ru.searcher.search;


import com.fasterxml.jackson.annotation.JsonProperty;

public class SearchDoc {

    @JsonProperty("Name")
    private String name;
    @JsonProperty("Type")
    private String type;
    @JsonProperty("Designed by")
    private String designedBy;

    public SearchDoc() {
    }

    public SearchDoc(String name, String type, String designedBy) {
        this.name = name;
        this.type = type;
        this.designedBy = designedBy;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getDesignedBy() {
        return designedBy;
    }

    public enum SearchFields {

        NAME("Name"),
        TYPE("Type"),
        DESIGNED_BY("Designed by");

        private final String fieldName;

        SearchFields(String fieldName) {
            this.fieldName = fieldName;
        }

        public String getFieldName() {
            return fieldName;
        }
    }

    @Override
    public String toString() {
        return "SearchDoc{" +
                "name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", designedBy='" + designedBy + '\'' +
                '}';
    }
}
