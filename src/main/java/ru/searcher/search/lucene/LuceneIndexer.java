package ru.searcher.search.lucene;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.searcher.config.Configuration;
import ru.searcher.search.Indexer;
import ru.searcher.search.SearchDoc;
import ru.searcher.search.SearchException;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;


public class LuceneIndexer implements Indexer {

    private final Configuration configuration;

    private static final Logger LOG = LoggerFactory.getLogger(LuceneIndexer.class);

    @Inject
    public LuceneIndexer(Configuration configuration) {
        this.configuration = configuration;
    }

    @Override
    public void createIndex() throws SearchException {
        addDocuments(parseSearchDocs());
    }

    private List<SearchDoc> parseSearchDocs() throws SearchException {
        try (InputStream in = Files.newInputStream(Paths.get(configuration.getJSONFilePath()))) {
            return new ObjectMapper().readValue(in, new TypeReference<List<SearchDoc>>() {
            });
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
    }

    private boolean addDocuments(List<SearchDoc> searchDocs) {
        try {
            final Directory dir =
                    FSDirectory.open(Paths.get(configuration.getIndexPath()).toFile());
            final Analyzer analyzer =
                    new StandardAnalyzer(Version.LUCENE_48);
            final IndexWriterConfig iwc =
                    new IndexWriterConfig(Version.LUCENE_48, analyzer);

            iwc.setOpenMode(OpenMode.CREATE);

            IndexWriter indexWriter = new IndexWriter(dir, iwc);
            searchDocs.stream().forEach(sd -> {
                Document doc = new Document();
                doc.add(new StringField(SearchDoc.SearchFields.NAME.getFieldName(), sd.getName(), Field.Store.YES));
                doc.add(new StringField(SearchDoc.SearchFields.TYPE.getFieldName(), sd.getType(), Field.Store.YES));
                doc.add(new StringField(SearchDoc.SearchFields.DESIGNED_BY.getFieldName(), sd.getDesignedBy(), Field.Store.YES));
                try {
                    indexWriter.addDocument(doc);
                } catch (IOException e) {
                    LOG.error(e.getMessage(), e);
                }
            });

            indexWriter.commit();
            indexWriter.close();
            return true;
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        return false;
    }
}