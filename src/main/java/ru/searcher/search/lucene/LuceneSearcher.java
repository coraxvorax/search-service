package ru.searcher.search.lucene;


import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.sandbox.queries.regex.RegexQuery;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.searcher.config.Configuration;
import ru.searcher.search.SearchDoc;
import ru.searcher.search.Searcher;

import javax.inject.Inject;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static ru.searcher.search.SearchDoc.SearchFields.*;

public class LuceneSearcher implements Searcher {

    private final Configuration configuration;

    private static final Logger LOG = LoggerFactory.getLogger(LuceneSearcher.class);

    @Inject
    public LuceneSearcher(Configuration configuration) {
        this.configuration = configuration;
    }

    @Override
    public List<SearchDoc> findDocs(String text) {
        try {
            final Directory indexDirectory =
                    FSDirectory.open(Paths.get(configuration.getIndexPath()).toFile());
            final IndexReader indexReader = DirectoryReader.open(indexDirectory);
            final IndexSearcher indexSearcher = new IndexSearcher(indexReader);
            final BooleanQuery query = new BooleanQuery();

            //TODO: hellish refactoring for using multifield correct search
            createRegexParts(text).stream().forEach(regex -> {
                query.add(new RegexQuery(new Term(TYPE.getFieldName(), regex)),
                        BooleanClause.Occur.SHOULD);
                query.add(new RegexQuery(new Term(NAME.getFieldName(), regex)),
                        BooleanClause.Occur.SHOULD);
                query.add(new RegexQuery(new Term(DESIGNED_BY.getFieldName(), regex)),
                        BooleanClause.Occur.SHOULD);
            });

            return Arrays.stream(indexSearcher.search(query, 10).scoreDocs)
                    .map(td -> {
                        try {
                            final Document document = indexSearcher.doc(td.doc);
                            return new SearchDoc(
                                    document.get(TYPE.getFieldName()),
                                    document.get(NAME.getFieldName()),
                                    document.get(DESIGNED_BY.getFieldName())
                            );
                        } catch (IOException e) {
                            LOG.error(e.getMessage(), e);
                            return new SearchDoc();
                        }
                    }).collect(Collectors.toList());
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        }
        return new ArrayList<>();
    }

    private List<String> createRegexParts(String text) {
        return Arrays.stream(text.split("\\s"))
                .map(t -> ".*" + t + ".*")
                .collect(Collectors.toList());
    }
}
