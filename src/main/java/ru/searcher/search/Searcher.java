package ru.searcher.search;


import java.util.List;

public interface Searcher {

    List<SearchDoc> findDocs(String text);
}
