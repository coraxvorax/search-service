package ru.searcher.search;


public interface Indexer {
    void createIndex() throws SearchException;
}
