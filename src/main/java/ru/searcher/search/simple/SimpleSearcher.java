package ru.searcher.search.simple;

import ru.searcher.search.SearchDoc;
import ru.searcher.search.Searcher;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by user on 08.06.2017.
 */
public class SimpleSearcher implements Searcher {

    private final SimpleStore simpleStore;

    @Inject
    public SimpleSearcher(SimpleStore simpleStore) {
        this.simpleStore = simpleStore;
    }

    @Override
    public List<SearchDoc> findDocs(String text) {
        final List<SearchDoc> searchDocs = new ArrayList<>();
        final List<String> regexParts =
                createRegexParts(text);
        final List<String> negativeRegexParts =
                createNegativeRegexParts(text);

        simpleStore.getDocs().entrySet()
                .stream().forEach(e -> {
            //TODO: if all true -> true
            int needRegexCount = 0;
            final SearchDoc sd = e.getValue();
            for (String regex : regexParts) {
                if (sd.getName().matches(regex) ||
                        sd.getType().matches(regex) ||
                        sd.getDesignedBy().matches(regex)) {
                    needRegexCount++;
                }
            }

            int needNegatives = 0;
            for (String negativeRegex : negativeRegexParts) {
                if (!sd.getName().matches(negativeRegex) &&
                        !sd.getType().matches(negativeRegex) &&
                        !sd.getDesignedBy().matches(negativeRegex)) {
                    needNegatives++;
                }
            }

            if ((needRegexCount == regexParts.size()) &&
                    (needNegatives == negativeRegexParts.size())) {
                searchDocs.add(sd);
            }
        });

        return searchDocs;
    }

    private List<String> createNegativeRegexParts(String text) {
        final String[] quoteDivided = text.split("\"");
        final List<String> regexParts = new ArrayList<>();

        IntStream.range(0, quoteDivided.length)
                .forEach(i -> {
                    if (i % 2 != 0 && !quoteDivided[i].isEmpty()) {
                        regexParts.add(quoteDivided[i]);
                    } else if (!quoteDivided[i].isEmpty()) {
                        regexParts.addAll(Arrays.stream(quoteDivided[i].split("\\s"))
                                .filter(s -> !s.isEmpty())
                                .collect(Collectors.toList()));
                    }

                });

        return regexParts.stream()
                .filter(rp -> rp.startsWith("-"))
                .map(t -> ".*" + t.substring(1) + ".*")
                .collect(Collectors.toList());
    }

    private List<String> createRegexParts(String text) {
        final String[] quoteDivided = text.split("\"");
        final List<String> regexParts = new ArrayList<>();

        IntStream.range(0, quoteDivided.length)
                .forEach(i -> {
                    if (i % 2 != 0 && !quoteDivided[i].isEmpty()) {
                        regexParts.add(quoteDivided[i]);
                    } else if (!quoteDivided[i].isEmpty()) {
                        regexParts.addAll(Arrays.stream(quoteDivided[i].split("\\s"))
                                .filter(s -> !s.isEmpty())
                                .collect(Collectors.toList()));
                    }

                });

        return regexParts.stream()
                .filter(rp -> !rp.startsWith("-"))
                .map(t -> ".*" + t + ".*")
                .collect(Collectors.toList());
    }
}
