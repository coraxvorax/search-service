package ru.searcher.search.simple;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.searcher.config.Configuration;
import ru.searcher.search.Indexer;
import ru.searcher.search.SearchDoc;
import ru.searcher.search.SearchException;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.IntStream;


public class SimpleIndexer implements Indexer {

    private final SimpleStore simpleStore;
    private final Configuration configuration;

    private static final Logger LOG = LoggerFactory.getLogger(SimpleIndexer.class);

    @Inject
    public SimpleIndexer(SimpleStore simpleStore, Configuration configuration) {
        this.simpleStore = simpleStore;
        this.configuration = configuration;
    }

    @Override
    public void createIndex() throws SearchException {
        addDocuments(parseSearchDocs());
    }

    private List<SearchDoc> parseSearchDocs() throws SearchException {
        try (InputStream in = Files.newInputStream(Paths.get(configuration.getJSONFilePath()))) {
            return new ObjectMapper().readValue(in, new TypeReference<List<SearchDoc>>() {
            });
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
            throw new SearchException(e.getMessage(), e);
        }
    }

    private void addDocuments(List<SearchDoc> searchDocs) {
        try {
            IntStream.range(0, searchDocs.size())
                    .forEach(i -> simpleStore.putToStore(i, searchDocs.get(i)));
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
    }
}
