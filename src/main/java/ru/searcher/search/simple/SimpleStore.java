package ru.searcher.search.simple;

import ru.searcher.search.SearchDoc;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public class SimpleStore {

    private final Map<Integer, SearchDoc> docs = new ConcurrentHashMap<>();

    public void putToStore(Integer docID, SearchDoc doc) {
        docs.put(docID, doc);
    }

    public Map<Integer, SearchDoc> getDocs() {
        return new ConcurrentHashMap<>(docs);
    }
}
