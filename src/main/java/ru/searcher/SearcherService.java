package ru.searcher;

import ch.qos.logback.access.servlet.TeeFilter;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import com.google.inject.name.Names;
import com.sun.jersey.api.core.ResourceConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import org.eclipse.jetty.server.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.searcher.config.Configuration;
import ru.searcher.config.SearchConfiruration;
import ru.searcher.guice.IndexerProvider;
import ru.searcher.guice.MainModule;
import ru.searcher.guice.SearcherProvider;
import ru.searcher.guice.meta.Default;
import ru.searcher.guice.web.AbstractContextListener;
import ru.searcher.search.Indexer;
import ru.searcher.search.Searcher;
import ru.searcher.search.simple.SimpleStore;
import ru.searcher.server.api.SearchAPI;
import ru.searcher.servers.ConfiguredServer;
import ru.searcher.servers.jetty.WebServerException;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.EventListener;


public class SearcherService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SearcherService.class);
    private SearchServer searchServer;

    public static class SearchListener extends AbstractContextListener {

        @Inject
        private Configuration configuration;

        @Override
        protected APIBuilder buildAPI() throws InstantiationException, IllegalAccessException {
            return new APIBuilder()
                    .servlet()
                    .basePath("/applicants/*")
                    .filter("/applicants/*", new TeeFilter())
                    .set(SearchAPI.class)
                    .setParam(JSONConfiguration.FEATURE_POJO_MAPPING, "true")
                    .setParam(
                            ResourceConfig.PROPERTY_MEDIA_TYPE_MAPPINGS,
                            String.format("xml:%s,json:%s",
                                    MediaType.APPLICATION_XML_TYPE,
                                    MediaType.APPLICATION_JSON_TYPE
                            )
                    ).buildServlet();
        }
    }

    public static class SearchServer extends ConfiguredServer {

        @Inject
        public SearchServer(@Named("jetty.config") String config,
                            @Named("jetty.log.config") String logConfig,
                            @Default EventListener eventListener)
                throws WebServerException {
            super(config, eventListener, logConfig);
        }

        @Override
        protected void initSessionIdManager(Server server) {
            //NOP
        }
    }

    public void init(String[] params) {
        try {
            final Injector searchServiceInjector = Guice.createInjector(
                    new MainModule(),
                    new AbstractModule() {
                        @Override
                        protected void configure() {
                            try {
                                bind(Configuration.class)
                                        .toInstance(new SearchConfiruration(
                                                Paths.get(ClassLoader.getSystemResource("config.main.yml")
                                                        .toURI())));
                            } catch (IOException | URISyntaxException e) {
                                LOGGER.error(e.getMessage(), e);
                            }
                            bind(String.class)
                                    .annotatedWith(Names.named("jetty.config"))
                                    .toInstance("/jetty.xml");
                            try {
                                bind(String.class)
                                        .annotatedWith(Names.named("jetty.log.config"))
                                        .toInstance(Paths.get(ClassLoader.getSystemResource("jetty.log.xml").toURI())
                                                .toAbsolutePath().toString());
                            } catch (URISyntaxException e) {
                                LOGGER.debug(e.getMessage(), e);
                            }
                        }
                    },
                    new AbstractModule() {
                        @Override
                        protected void configure() {
                            bind(EventListener.class)
                                    .annotatedWith(Default.class)
                                    .to(SearchListener.class);
                            bind(Indexer.class)
                                    .toProvider(IndexerProvider.class)
                                    .in(Singleton.class);
                            bind(Searcher.class)
                                    .toProvider(SearcherProvider.class)
                                    .in(Singleton.class);
                            bind(SimpleStore.class)
                                    .in(Singleton.class);
                        }
                    }
            );
            final Indexer indexer = searchServiceInjector.getInstance(Indexer.class);
            indexer.createIndex();

            searchServer = searchServiceInjector.getInstance(SearchServer.class);
            searchServer.configure();
            searchServer.start();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            e.printStackTrace();
        }
    }

    public void stop() {
        try {
            searchServer.stop();
        } catch (WebServerException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    public static void main(String[] args) {
        final SearcherService searcherService = new SearcherService();
        searcherService.init(args);
    }

    public enum SearchStrategy {
        LUCENE,
        SIMPLE
    }
}
