package ru.searcher.search.lucene;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import ru.searcher.config.Configuration;
import ru.searcher.search.SearchException;
import ru.searcher.search.lucene.LuceneIndexer;
import ru.searcher.search.lucene.LuceneSearcher;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;


public class LuceneSearcherTest {

    //TODO: do tests

    private LuceneSearcher searcher;
    private Configuration configuration;

    @Before
    public void setUp() throws Exception {
        this.configuration = getMockConfiguration();
        initDocs(configuration);
        this.searcher = spy(new LuceneSearcher(configuration));
    }

    @Test
    public void shouldFindCorrectNumberOfDocs() {
        assertThat(searcher.findDocs("Compiled").size(), is(10));
    }

    private Configuration getMockConfiguration()
            throws URISyntaxException {
        final Configuration configuration = mock(Configuration.class);
        when(configuration.getIndexPath())
                .thenReturn("indexDir");
        when(configuration.getJSONFilePath())
                .thenReturn("data/data.json");
        return configuration;
    }

    private void initDocs(Configuration configuration) throws SearchException {
        final LuceneIndexer indexer = new LuceneIndexer(configuration);
        indexer.createIndex();
    }

    @AfterClass
    public static void tearDown(){
        try {
            Files.walkFileTree(Paths.get("indexDir"), new FileVisitor<Path>() {

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException exc) {
                    System.out.println("deleting directory after test:" + dir);
                    try {
                        Files.delete(dir);
                    } catch (IOException e) {
                        System.err.printf("Can't delete directory %s : %s\n", dir, e.getMessage());
                    }
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult preVisitDirectory(Path dir,
                                                         BasicFileAttributes attrs) throws IOException {
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFile(Path file,
                                                 BasicFileAttributes attrs) {
                    System.out.println("Deleting file after test: " + file);
                    try {
                        Files.delete(file);
                    } catch (IOException e) {
                        System.err.printf("Can't delete file %s : %s\n", file, e.getMessage());
                    }
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFileFailed(Path file, IOException exc)
                        throws IOException {
                    System.out.println(exc.toString());
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException e) {
            System.err.printf("Error when deleting index directory %s after test: %s\n", "indexDir", e.getMessage());
        }

    }
}