package ru.searcher.search.simple;

import org.junit.Test;
import ru.searcher.search.SearchDoc;

import java.util.List;

/**
 * Created by user on 08.06.2017.
 */
public class SimpleSearcherTest {

    //TODO: do tests
    @Test
    public void should() {
        final SimpleStore simpleStore = new SimpleStore();
        simpleStore.putToStore(1, new SearchDoc("super", "infra mega", "low"));
        simpleStore.putToStore(2, new SearchDoc("", "infra mega", "low"));
        simpleStore.putToStore(3, new SearchDoc("super", "mega", "low"));
        simpleStore.putToStore(4, new SearchDoc("infra", "low", ""));
        final SimpleSearcher searcher = new SimpleSearcher(simpleStore);

        final List<SearchDoc> docList = searcher.findDocs("low mega");
        System.out.println(docList);
    }
}