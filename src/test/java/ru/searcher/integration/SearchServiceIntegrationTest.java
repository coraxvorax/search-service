package ru.searcher.integration;

import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.ClassRule;
import org.junit.Test;
import ru.searcher.integration.rules.TestSearchServerRule;
import ru.searcher.server.entity.SearchRequest;
import ru.searcher.servers.jetty.WebServerException;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;


public class SearchServiceIntegrationTest {

    @ClassRule
    public static final TestSearchServerRule SEARCH_SERVER_RULE = new TestSearchServerRule();

    //TODO: do tests

    @Test
    public void shouldSuccessSendRequest() throws WebServerException, IOException {
        assertThat(Request.Post(TestSearchServerRule.SEARCH_URL)
                .bodyString(new ObjectMapper()
                                .writeValueAsString(new SearchRequest("ActionScript")),
                        ContentType.APPLICATION_JSON)
                .addHeader("Test", "for tests")
                .addHeader("User-Agent", "test-agent")
                .execute()
                .returnResponse().getStatusLine().getStatusCode(), is(200));
    }
}
