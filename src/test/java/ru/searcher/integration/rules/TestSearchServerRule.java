package ru.searcher.integration.rules;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import com.google.inject.name.Names;
import org.junit.rules.ExternalResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.searcher.SearcherService;
import ru.searcher.config.Configuration;
import ru.searcher.config.SearchConfiruration;
import ru.searcher.guice.MainModule;
import ru.searcher.guice.meta.Default;
import ru.searcher.search.*;
import ru.searcher.search.lucene.LuceneIndexer;
import ru.searcher.search.lucene.LuceneSearcher;
import ru.searcher.servers.jetty.WebServerException;

import java.util.EventListener;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestSearchServerRule extends ExternalResource {

    public final static String SEARCH_URL = "http://localhost:6883/applicants/search/document";

    private static final Logger LOGGER = LoggerFactory.getLogger(SearcherService.class);
    private SearcherService.SearchServer searchServer;

    @Override
    protected void before() throws WebServerException, SearchException {
        Injector searchServiceInjector = Guice.createInjector(
                new MainModule(),
                new AbstractModule() {
                    @Override
                    protected void configure() {
                        SearchConfiruration confiruration = mock(SearchConfiruration.class);
                        when(confiruration.getIndexPath())
                                .thenReturn("indexDir");
                        when(confiruration.getJSONFilePath())
                                .thenReturn("data/data.json");
                        bind(Configuration.class)
                                .toInstance(confiruration);
                        bind(String.class)
                                .annotatedWith(Names.named("jetty.config"))
                                .toInstance("/jetty.xml");
                        bind(String.class)
                                .annotatedWith(Names.named("jetty.log.config"))
                                .toInstance("conf/jetty.log.xml");
                    }
                },
                new AbstractModule() {
                    @Override
                    protected void configure() {
                        bind(EventListener.class)
                                .annotatedWith(Default.class)
                                .to(SearcherService.SearchListener.class);
                        bind(Indexer.class)
                                .to(LuceneIndexer.class);
                        bind(Searcher.class)
                                .to(LuceneSearcher.class)
                                .in(Singleton.class);
                        bind(SearcherService.SearchServer.class)
                                .in(Singleton.class);
                    }
                }
        );
        final Indexer indexer = searchServiceInjector.getInstance(Indexer.class);
        indexer.createIndex();

        searchServer = searchServiceInjector.getInstance(SearcherService.SearchServer.class);
        searchServer.configure();
        searchServer.start();
    }

    @Override
    protected void after() {
        try {
            if (searchServer != null) {
                searchServer.stop();
            }
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }
}
